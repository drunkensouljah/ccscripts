--[[

WIP

ToDo:
- Slot auswahl
- fix Replace Funktion
- fix Tank Funktion

Turtle baut Mauer.
Script fuer Mining Turtle.
]]--

--Functions
----------------------------------------------------------------------------

local function hilfe()
	term.setTextColor(colors.black)
	term.setBackgroundColor(colors.white)
	print("Hilfe:")
	term.clearLine()
	term.setTextColor(colors.white)
	term.setBackgroundColor(colors.black)
	term.clearLine()
	print("mauer <l\195\164nge> <h\195\182he>")
	print(" ")
	print("Lege Mauergr\195\182sse fest.")
	print("Ben\195\182tigt Mining Turtle!")
	return
 end

function geradeaus()
	while turtle.detect() do
		turtle.dig()
		sleep(0.5) -- Evtl. runterfallender Sand oder Gravel wird abgebaut.
	end

	if not turtle.forward() then 
		while not turtle.forward() do
			turtle.attack()
			sleep(0.5)
		end
	end
 end

function nachOben()
	while turtle.detectUp() do
		turtle.digUp()
		sleep(0.5)
	end

	if not turtle.up() then
		while not turtle.up() do
			turtle.attackUp()
			sleep(0.5)
		end
	end
 end

function mauerBauen()
	if turtle.detectDown() then
		turtle.digDown()
	end
	turtle.select(1)
	turtle.placeDown()
 end

----------------------------------------------------------------------------

--Main Program

if not turtle then
	print("FEHLER: Du kannst das Programm nur auf einem Miningturtle starten!")
 end

local tArgs = { ... }
local lang = tonumber( tArgs[1] )
local hoch  = tonumber( tArgs[2] )

-- verhindert ungueltige Zahlen
if lang == nil or hoch == nil or lang <= 0 or hoch <= 0 then
  	hilfe()
  	return
 end

local mauergroesse = lang * hoch

term.clear()
term.setCursorPos(1, 2)
print("Mauer wird gebaut: " .. lang .. " x " .. hoch .. " Block" )
term.setCursorPos(1, 4)
print("Es werden " .. mauergroesse .. " Bl\195\182cke ben\195\182tigt.")

local itemCount = 0
  	for i=1,15,1 do
   		itemCount = itemCount + turtle.getItemCount(i)
  	end

if itemCount < mauergroesse then
	term.setCursorPos(1, 6)
	print("Lege Baumaterial in die Slots 1-15.")
	local itemFehlt = mauergroesse - itemCount
	print("Du brauchst noch "..itemFehlt.." Bl\195\182cke")
 end

local benzinstand = turtle.getFuelLevel()

function auftanken()
	while benzinstand < mauergroesse do
		turtle.select(16)
		turtle.refuel(1)
		benzinstand = turtle.getFuelLevel()
		if not turtle.refuel(1) then
			return false
		end
	end
	return true
 end

if benzinstand < mauergroesse then
	term.setCursorPos(1, 9)
	print("Turtle muss erst betankt werden. Benzinstand: " .. benzinstand)
	print("Lege Treibstoff in Slot 16.")
	turtle.select(16)
	local success = auftanken()
	if not success then
		print("Nicht genug Treibstoff")
		return -- end program
	end
end

term.setCursorPos(5, 13)
print("[ BELIEBIGE TASTE ZUM STARTEN ]")
os.pullEvent( "key" )

term.clear()
term.setCursorPos(1, 1)
term.setTextColor(colors.black)
term.setBackgroundColor(colors.white)
term.clearLine()
print("Mauer wird gebaut: " .. lang .. " x " .. hoch.." ...")
term.setTextColor(colors.white)
term.setBackgroundColor(colors.black)
term.clearLine()

term.setCursorPos(1, 3)
print("Mauer wird gebaut.")

-- AB HIER GEHT ES AB --

turtle.select(1)
nachOben()
 
for aktuelleHoehe = 1, hoch do

	for aktuelleLaenge = 1, lang do
    	mauerBauen()
    	if aktuelleLaenge ~= lang then
      		geradeaus()
    	end
  	end
 
  	if aktuelleHoehe ~= hoch then
    	turtle.turnRight()
    	turtle.turnRight()
    	nachOben()
  	end
 end

print("Mauer erfolgreich gebaut.")
