-- functions
function mainProgramm(args)
	--pastebin get NFBKxg0v GeneratorTest
	shell.run("clear");
	print("----------------------------------");

	local argOne = args[1];

	if argOne == "update" then
		update();
		return;
	end

	if argOne == "print" then
		while true do
			local states = ReadGeneratorStatus();
			PrintGeneratorStates(states);
			print("starting next iteration");
		end
	end

	--print("Reading Generator File");
	gStates = ReadGeneratorStatus();
	--print("----------------------------------");
	PrintGeneratorStates(gStates);
	--print("----------------------------------");
end

function update()
	print("Renewing program");
	shell.run("renew", "genTest", "NFBKxg0v");
	shell.run("genTest", "true");
end

function PrintGeneratorStates(states)

	shell.run("clear");
	monitor = peripheral.wrap("right");
	--monitor.setTextScale(1);

	--term.clear();
	--term.setCursorPos(1,1);
	--term.setCursorPos(1,3);
	for index,value in ipairs(states) do
		local generator = value:toString();
		print(generator);

		local word = nil;
		if 1 == value.Status then
			word = "An";
		else
			word = "Aus";
		end

		print("Reihe "..value.ID.." "..word);
	end
	sleep(15); --only for testing wait 15 s. Maybe 30 when finished
end

function WriteGeneratorStatus(states)
	if states == nil then
		return;
	end

	if not fs.exists(dirName) then
		fs.makeDir(dirName);
	end
	local size = table.getn(states);
	if 0 == size then
		print("No data written.");
		return;
	end
	local file = fs.open(statusFileName, "w");
	for index,value in ipairs(states) do
		local generator = value:toString();
		file.writeLine(generator);
	end
	file.close();
	file = nil;
end


function registerGenerator(generator)

	if generator == nil then
		return;
	end
	gStates = nil;
	gStates = ReadGeneratorStatus();
	local pos = table.getn(gStates);
	table.insert(gStates, generator)
end

function ReadGeneratorStatus()
	if not fs.exists(dirName) then
		fs.makeDir(dirName);
	end

	local statusFile = fs.open(statusFileName, "r");

	if statusFile == nil then
		print("Status file not found.");
		return;
	end

	local count = 0;
	local generators = {};
	local line = statusFile.readLine();
	repeat
		if line == nil then
			break;
		end
		tmpGenerator = Generator:new();
		tmpGenerator:fillFromString(line);
		if tmpGenerator ~= nil then
			table.insert(generators, tmpGenerator);
			--print(string.format("%6d  ", count), line, "\n");
			count = count + 1
		end
		tmpGenerator = nil;
		line = statusFile.readLine();
	until line == nil

	--print("Read ",count," lines from File.");
	if statusFile ~= nil and statusFile.close() ~= nil then
		statusFile.Close();
	end
	return generators;
end


Generator = {
	ID = 0,
	Status = 0,
	CableColor = 0,
	CableDir = "",
	toString = function (this)
		return string.format(
			"{ID=%d; Status=%d; CableColor=%d; CableDir=%s;}",
			this.ID,
			this.Status,
			this.CableColor,
			this.CableDir);
	end,
	new = function(this, o)
		o = o or {};
		setmetatable(o, this);
		this.__index = this;
		return o;
	end,
	fillFromString = function (this, input)
		--[[
			An input could look like
			{ID=1001;Status=0; CableColor=32; CableDir=Bottom;}
			or
			ID=5; Status=1; CableColor=64; CableDir=Left;

			Find the regex patterns and take the third
			result value (first capture of the regex)
			and store the content in the according member
		--]]
		idString = select(3, string.find(input, "ID=([%d]+);"));
		this.ID = tonumber(idString);
		local statusString = select(3, string.find(input, "Status=([0-1]);"));
		this.Status = tonumber(statusString);
		local colorString = select(3, string.find(input, "CableColor=([%d]+)"));
		this.CableColor = tonumber(colorString);
		local dirString = select(3, string.find(input, "CableDir=([%a]*);"));
		this.CableDir = dirString;
	end,
	new = function(this, o)
		o = o or {};
		setmetatable(o, this);
		this.__index = this;
		return o;
	end
}

--End



--Execution part
dirName = "CobblestoneGenerator";
statusFileName = "CobblestoneGenerator/GeneratorStatus";
gStates = {}

tArgs = {...};
mainProgramm(tArgs);
--End

