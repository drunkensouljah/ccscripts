-- Startbildschirm GUI

-- EINSTELLUNGEN
statusFarbe = 256
statustextFarbe = 16384
titeltextFarbe = 4096
hintergrundFarbe = 8
kontextmenuHintergrund = 128
kontextmenuText = 256
xStatusFarbe = 16384
xStatusTextFarbe = 1
hintergrundbild = false


-- nicht aendern
menu = 0

term.setBackgroundColor(hintergrundFarbe)
term.clear()

--Statusleiste
function statusLeiste()
	term.setCursorPos(1, 1)
	term.setBackgroundColor(statusFarbe)
	term.clearLine()
	term.setCursorPos(2, 1)
	term.setBackgroundColor(xStatusFarbe)
	term.setTextColor(xStatusTextFarbe)
	print(" X ")
end

function bildschirmHintergrund()
  term.setBackgroundColor(hintergrundFarbe)
  term.clear()
  if hintergrundbild then
     bground = paintutils.loadImage(".background")
     paintutils.drawImage(bground,1,1)
  end
  statusLeiste()
  term.setCursorPos(20, 6)
  print("test")
end

function kontextMenu()
  term.setTextColor(kontextmenuText)
  term.setBackgroundColor(kontextmenuHintergrund)
  term.setCursorPos(1,2)
  print("              ")
  term.setCursorPos(1,3)
  print(" Runterfahren ")
  term.setCursorPos(1,4)
  print(" Neustart     ")
  term.setCursorPos(1,5)
  print("              ")
 
end

bildschirmHintergrund()

while true do
local event, button, X,Y = os.pullEventRaw()
  if menu == 0 then
    if event == "mouse_click" then
      if X >= 1 and X <= 4 and Y == 1 and button == 1 then
      kontextMenu()
      menu = 1
        else
        bildschirmHintergrund()
      end
    end
   elseif menu == 1 then
     if X >= 1 and X <= 15 and Y == 3 and button == 1 then menu = 0 
       os.shutdown()
       elseif X >= 1 and X <= 15 and Y == 4 and button == 1 then menu = 0
       os.reboot()
       else
       menu = 0
       bildschirmHintergrund()
     end 
  end
end