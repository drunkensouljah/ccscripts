local sPath = ".:/rom/programs"
if turtle then
        sPath = sPath..":/rom/programs/turtle"
        sPath = sPath..":/Turtle"
else
        sPath = sPath..":/rom/programs/computer"
        sPath = sPath..":/lib"
end
if http then
        sPath = sPath..":/rom/programs/http"
end
if term.isColor() then
        sPath = sPath..":/rom/programs/color"
end
 
shell.setPath( sPath )
help.setPath( "/rom/help" )
 
shell.setAlias( "ls", "list" )
shell.setAlias( "dir", "list" )
shell.setAlias( "cp", "copy" )
shell.setAlias( "mv", "move" )
shell.setAlias( "rm", "delete" )
shell.setAlias( "pb", "pastebin")
shell.setAlias( "dl", "/lib/download")
shell.setAlias( "bb", "update")     -- installiert/aktualisiert Bitbucket Repository

 
if fs.exists( "/rom/autorun" ) and fs.isDir( "/rom/autorun" ) then
        local tFiles = fs.list( "/rom/autorun" )
        table.sort( tFiles )
        for n, sFile in ipairs( tFiles ) do
                if string.sub( sFile, 1, 1 ) ~= "." then
                        local sPath = "/rom/autorun/"..sFile
                        if not fs.isDir( sPath ) then
                                shell.run( sPath )
                        end
                end
        end
end
 
 
-- Erkennt Modem und aktiviert dies
local function openRednet()
  for _,side in ipairs({"top", "bottom", "front", "left", "right", "back"}) do
        if peripheral.isPresent(side) and peripheral.getType(side) == "modem" then
          rednet.open(side)
          return side
        end
  end
end
 
 
-- Stellt sicher dass der Computer mit einem Label versehen wird.
if not os.getComputerLabel() then
  write("Es wurde noch kein Label gesetzt. Bitte gebe dem Ger\195\164t einen Namen:")
  os.setComputerLabel(read())
  os.reboot()
end
 
term.clear()
term.setCursorPos(1, 1)
 
-- Prueft ob es sich um einen Advanced Computer handelt.
if term.isColor and term.isColor() then
        term.setTextColor(colors.orange)
        write("Label: " .. os.getComputerLabel())
        term.setTextColor(colors.white)
        write(" - ")
        term.setTextColor(colors.orange)
        print("Advanced Computer #" .. os.getComputerID())
        term.setTextColor(colors.yellow)
        write(os.version())
        modemSide = openRednet()
        if modemSide == nil then
                term.setTextColor(colors.red)
                print(" - Kein Modem")
        else
                term.setTextColor(colors.green)
                print(" - Modem: "..modemSide)
        end
-- Erkennt ob es sich um einen Turtle handelt
elseif turtle then 
        term.setBackgroundColor(colors.white)
        term.clearLine()
        term.setTextColor(colors.black)
        print("Label: " .. os.getComputerLabel() .. " - Turtle #" .. os.getComputerID())
        term.clearLine()
        print(os.version() .. " - Benzinstand: " .. turtle.getFuelLevel())
        modemSide = openRednet()
-- Erkennt ob es sich um einen normalen Computer handelt (nur Schwarz und Weiss)
elseif term.isColor then
        term.setBackgroundColor(colors.white)
        term.clearLine()
        term.setTextColor(colors.black)
        print("Label: " .. os.getComputerLabel() .. " - " .. "Copmuter #" .. os.getComputerID())
    	term.clearLine()
        write(os.version())
        modemSide = openRednet()
        if modemSide == nil then
                print(" - Kein Modem")
        else
                print(" - Modem: "..modemSide)
        end
-- Computer vor CC 1.4 (ohne Farben)
else
        print("Label: " .. os.getComputerLabel() .. " - " .. "Computer #" .. os.getComputerID())
        write(os.version())
        modemSide = openRednet()
        if modemSide == nil then
                print(" - Kein Modem")
        else
                print(" - Modem: "..modemSide)
        end
end