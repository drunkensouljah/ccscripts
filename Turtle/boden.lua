--[[
Turtle verlegt Boden.
Script fuer Mining Turtle.
]]--

--Functions
----------------------------------------------------------------------------

local function hilfe()
	print("Hilfe: boden <l\195\164nge> <breite>")
	print(" ")
	print("Lege fest wie gross der Flur werden soll (Nach vorne und nach rechts).")
	print("Ben\195\182tigt Mining Turtle!")
	return
end

function geradeaus()
	while turtle.detect() do
		turtle.dig()
		sleep(0.5) -- Evtl. runterfallender Sand oder Gravel wird abgebaut.
	end

	if not turtle.forward() then -- Falls Turtle sich nicht vorwaerts bewegen kann weil z.B. ein Mob den Weg versperrt wartet er kurz und versucht es nochmal.
		while not turtle.forward() do
			sleep(1)
		end
	end
end

-- Wenn der Untergrund nicht schon aus dem Baumaterial (Referenz in Slot 1) besteht wird ein Block platziert
function legeBoden(slot)
	turtle.select(1)
	if not turtle.compareDown() then
		turtle.select(slot)
		turtle.placeDown()
	end
end

function legeBodenInventar()
	turtle.select(1)
	if turtle.getItemCount(1) > 1 then
		legeBoden(1)
		return true
	end
	for i = 2, 15 do
		if turtle.compareTo(i) then
			legeBoden(i)
			return true
		end
	end
	return false
end

function baueReihe(lang)
	for i = 1, lang do
		if turtle.detectDown() then turtle.digDown() end
		legeBodenInventar()
		if i ~= lang then geradeaus() end
	end
end


----------------------------------------------------------------------------

--Main Program

if not turtle then
	print("FEHLER: Du kannst das Programm nur auf einem Turtle starten!")
end

local tArgs = { ... }
local lang = tonumber( tArgs[1] )
local breit  = tonumber( tArgs[2] )

-- verhindert ungueltige Zahlen
if lang == nil or breit == nil or lang < 0 or breit < 0 then
  hilfe()
  return
end

local bodengroesse = lang * breit
local benzinstand = turtle.getFuelLevel()

term.clear()
term.setCursorPos(1, 4)
print("Boden wird verlegt: " .. lang .. " x " .. breit .. " Block" )
term.setCursorPos(1, 6)
print("Es werden " .. bodengroesse .. " Bl\195\182cke ben\195\182tigt.")
print("Lege Baumaterial in die Slots 1-15")

function auftanken()
	while benzinstand < bodengroesse do
		turtle.select(16)
		turtle.refuel(1)
		benzinstand = turtle.getFuelLevel()
		if not turtle.refuel(1) then
			return false
		end
	end
	return true
end

if benzinstand < bodengroesse then
	term.setCursorPos(1, 9)
	print("Turtle muss erst betankt werden. Benzinstand: " .. benzinstand)
	print("Lege Treibstoff in Slot 16.")
	turtle.select(16)
	local success = auftanken()
	if not success then
		print("Nicht genug Treibstoff")
		return -- end program
	end
end
term.setCursorPos(5, 13)
print("[ BELIEBIGE TASTE ZUM STARTEN ]")
os.pullEvent( "key" )
term.clear()

term.setCursorPos(1, 5)
print("Turtle macht sich an die Arbeit")

for aktuelleBreite = 1, breit do
	if aktuelleBreite == 1 then
		geradeaus()
	else
		if aktuelleBreite % 2 == 0 then
			turtle.turnRight()
			geradeaus()
			turtle.turnRight()
		else
			turtle.turnLeft()
			geradeaus()
			turtle.turnLeft()
		end
	end

	if baueReihe(lang) == false then
		print("Nicht genug Bl\195\182cke zum bauen!")
	end
end
print("Boden erfolgreich verlegt.")