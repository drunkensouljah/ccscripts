-- Author: DerTroglodyt http://www.computercraft.info/forums2/index.php?/topic/5134-recursive-download-from-bitbucket-repository/
-- Download des kompletten Bitbucket Repository.
--
-- Die Dateiendung .lua wird nicht mit gespeichert damit einfacher auf die Dateien zugegriffen werden kann.


-- The BitBucket API returns json objects. To use them in lua we need this api.
os.loadAPI("/lib/json")

local root = "https://api.bitbucket.org/1.0/repositories/drunkensouljah/ccscripts/src/master/"

function get(filename)
  local result = http.get(root .. filename)
  local data = result.readAll()
  result.close()
--  print(data)
  local obj = json.decode(data)
  return obj
end

function getLocalPath(dir)
  return "/" .. string.gsub(dir, "/src/", "/")
end

function getDir(dir)
  local localDir = getLocalPath(dir .. "/")
  if not fs.exists(localDir) then
    fs.makeDir(localDir)
  end
  local result = get(dir)
  for i=1, #result.files do
    if string.find(result.files[i].path, "README.md") then
      print("Ignoriere README.md")
    else
      print("Hole Datei " .. result.files[i].path .. "...")
      local fd = get(result.files[i].path)
      local filename = string.gsub(getLocalPath("/" .. result.files[i].path), ".lua", "")
      local file = fs.open(filename, "w")
      if not file then
        error("Fehler beim bearbeiten von <" .. filename .. ">")
      end
      file.write(fd.data)
      file.close()
    end
  end
  for i=1, #result.directories do
    print("Hole Verzeichnis " .. result.path .. result.directories[i] .. "...")
    getDir(result.path .. result.directories[i])
  end
end

local result = getDir("")
print("Fertig")