-- Download Tool fuer Download von verschiedenen Quellen.
-- Quellen: Pastebin, Gist, URL,Dropbox... geplant: Github, Bitbucket,...
 
 
--Functions
----------------------------------------------------------------------------

local function hilfe()
    print("Hilfe:")
    print("download pb <PASTEBIN ID> <DATEI>")
    print("download gist <GIST ID> <DATEI>")
    print("download url <URL> <DATEI>")
    print("download db <DROPBOX USER ID> <DATEI>")
 end
 
local function download(sConnect, sURL, sFile)
    local sPath = shell.resolve(sFile)
    if fs.exists(sPath) then
        print("Datei "..sFile.." existiert bereits.")
        print("Soll sie \195\188berschrieben werden? J/N")
        repeat 
            event,pram=os.pullEvent("char")
            until pram=="j" or pram=="n"
            if pram=="n" then
                print("Abgebrochen.")
                return
            end
     end
    write(sConnect)  
    local response = http.get(sURL)
    
    if response then
        print("Erfolgreich.")
            
        local sResponse = response.readAll()
        response.close()

        local file = fs.open( sPath, "w")
        file.write(sResponse)
        file.close()
        print("Gespeichert als " .. sFile)
    
    else
        print("Download fehlgeschlagen!")
    end
 end

----------------------------------------------------------------------------
--Main Program

 
if not http then
    term.clear()
    print("Dieses Programm ben\195\182tigt die http API")
    print("Stelle enableAPI_http auf 1 in den Modeinstellungen.")
    return
 end
 
tArgs = { ... }

if #tArgs < 1 then
    hilfe()
    return
 end

-- Hilfe
if tArgs[1] ==  "help" then
    hilfe()
    return
-- Pastebin
elseif tArgs[1] == "pb" then
    if #tArgs < 3 then
        hilfe()
        return
    end
    download("Verbinde mit Pastebin...", "http://www.pastebin.com/raw.php?i="..tArgs[2], tArgs[3])
    return
-- Gist
elseif tArgs[1] == "gist" then
    if #tArgs < 3 then
        hilfe()
        return
    end
    download("Verbinde mit GitHub/Gist...", "https://raw.github.com/gist/"..tostring(tArgs[2]), tArgs[3])
    return
-- URL
elseif tArgs[1] == "url" then
    if #tArgs < 3 then
        hilfe()
        return
    end
    download("Verbinde mit URL...", tArgs[2], tArgs[3])
    return
-- Dropbox
elseif tArgs[1] == "db" then
    if #tArgs < 3 then
        hilfe()
        return
    end
    download("Verbinde mit Dropbox...", "http://dl.dropbox.com/u/"..tostring(tArgs[2]), tArgs[3])
    return
-- Sonst Hilfe
else
    hilfe()
 end