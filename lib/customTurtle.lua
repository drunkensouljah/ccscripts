--customTurtle
--import script via os.loadAPI("lib/customTurtle")
customTurtle = {
	fuel,
	fillFuel = function(this, groundSize, slot)
		--initialze params with default value if not present
		groundSize = groundSize or 1;
		slot = slot or 16;
		while this.fuel < groundSize do
			turtle.select(slot)
			turtle.refuel(1)
			this.fuel = turtle.getFuelLevel()
			if not turtle.refuel(1) then
				return false
			end
		end
		return true
	end,
	moveForward = function(this)
		while turtle.detect() do
			turtle.dig()
			sleep(0.5) -- Evtl. runterfallender Sand oder Gravel wird abgebaut.
		end

		if not turtle.forward() then -- Falls Turtle sich nicht vorwaerts bewegen kann weil z.B. ein Mob den Weg versperrt wartet er kurz und versucht es nochmal.
			while not turtle.forward() do
				sleep(1)
			end
		end
	end,
	-- Wenn der Untergrund nicht schon aus dem Baumaterial (Referenz in Slot 1) besteht wird ein Block platziert
	placeBlock = function(this, slot)
		turtle.select(1)
		if not turtle.compareDown() then
			turtle.select(slot)
			turtle.placeDown()
		end
	end,
	placeBlockInventory = function(this)
		turtle.select(1)
		if turtle.getItemCount(1) > 1 then
			this:placeBlock(1)
			return true
		end
		for i = 2, 15 do
			if turtle.compareTo(i) then
				this:placeBlock(i)
				return true
			end
		end
		return false
	end,
	buildRow = function(this, length)
		for i = 1, lang do
			if turtle.detectDown() then 
				turtle.digDown() +
			end
			this:placeBlockInventory()
			if i ~= length then moveForward() end
		end
	end,
	makeFloor = function(this, length, width)
		for aktuelleBreite = 1, width do
			if aktuelleBreite == 1 then
				this:moveForward()
			else
				if aktuelleBreite % 2 == 0 then
					turtle.turnRight()
					this:moveForward()
					turtle.turnRight()
				else
					turtle.turnLeft()
					this:moveForward()
					turtle.turnLeft()
				end
			end
		end
		if this:buildRow(length) == false then
			print("Nicht genug Bl\195\182cke zum bauen!")
		end
		print("Boden erfolgreich verlegt.")
	end,
	makeWall = function(this, height, length)
		turtle.select(1)
		this:moveUp()
		 
		for aktuelleHoehe = 1, length do

			for aktuelleLaenge = 1, length do
				mauerBauen()
				if aktuelleLaenge ~= length then
					this:moveForward()
				end
			end
		 
			if aktuelleHoehe ~= length then
				turtle.turnRight()
				turtle.turnRight()
				moveUp()
			end
		 end

		print("Mauer erfolgreich gebaut.")
	end,
	printHelp = function()
		print("Hilfe: boden <l\195\164nge> <breite>");
		print(" ");
		print("Lege fest wie gross der Flur werden soll (Nach vorne und nach rechts).");
		print("Ben\195\182tigt Mining Turtle!");
	end,
	getFuelLevel = function(this)
		this.fuel = turtle.getFuelLevel();
		return fuel;
	end,
	getItemCount = function(this, slot)
		if slot then -- if slot is set return itemCount of that slot
			return turtle.getItemCount(slot);
		end,
		--otherwise calculate the complete itemCount except slot #16
		local itemCount = 0;
		for i=1,15,1 do
			itemCount = itemCount + turtle.getItemCount(i);
		end
		return itemCount;
	end,
	moveUp = function(this)
		while turtle.detectUp() do
			turtle.digUp()
			sleep(0.5)
		end

		if not turtle.up() then
			while not turtle.up() do
				turtle.attackUp()
				sleep(0.5)
			end
		end
	end,
	new = function(this, o)
		if not turtle then 
			return nil;
		end
		o = o or {};
		setmetatable(o, this);
		this.__index = this;
		this.fuel = turtle.getFuelLevel();
		return o;
	end
}