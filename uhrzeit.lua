-- Zeigt die aktuelle Spielzeit und optional die Tage an.


-- Konfiguration
-- Tage auf true um Spieltage anzuzeigen. Stunden24 auf true um 24 Stunden Anzeige zu haben. Sonst AM/PM.
local Stunden24 = true
local Tage = true 

-- Funktion um Text mittig auszurichten
local function mittigerText(text)
local x,y = term.getSize()
local x2,y2 = term.getCursorPos()
term.setCursorPos(math.ceil((x / 2) - (text:len() / 2)), y2)
print(text)
end


-- Uhr Funktion
while true do
   term.clear()
   term.setCursorPos(1,5)  
   mittigerText(textutils.formatTime(os.time(),Stunden24))
   if Tage then
      term.setCursorPos(1,7) 
      mittigerText("Tag:"..os.day())
   end
   sleep(1)
end